# talisman-cordova-plugin-inapppurchase

This repo is a clone of https://github.com/AlexDisler/cordova-plugin-inapppurchase with fixes from various sources.

## Dependency Notice

Version 2.0.0 and later of this plugin requires cordova-android@7.0.0 when Android platforms.
If you are still using cordova-android@6.x, then stick with version 1.x of this plug-in.
